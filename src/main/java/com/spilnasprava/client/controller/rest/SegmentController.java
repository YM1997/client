package com.spilnasprava.client.controller.rest;

import com.spilnasprava.client.service.segmentclient.SegmentClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.Segment;
import java.util.List;

/**
 * Created by YaroslaV on 23.03.2018.
 */
@RestController
@RequestMapping("rest/segments")
public class SegmentController {

    @Autowired
    private SegmentClient segmentClient;
    @GetMapping
    public Page<Segment> findAll(@RequestParam("user") String name,@RequestParam("page") int page, @RequestParam ("size") int size){
        return segmentClient.findAll(name,page,size);
    }

    @GetMapping("/by-city")
    public List<Segment> findByCity(@RequestParam("user") String name,@RequestParam ("city") String city){
        return segmentClient.findByCity(name,city);
    }

    @GetMapping("/by-points")
    public List<Segment> findByPoints(@RequestParam("user") String name,@RequestParam("lat") double lat, @RequestParam("lon") double lon, @RequestParam("radius") double radius ){
        return segmentClient.findByPoints(name,lon, lat, radius);
    }
}
