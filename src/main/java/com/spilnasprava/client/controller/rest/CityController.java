package com.spilnasprava.client.controller.rest;

import com.spilnasprava.client.model.City;
import com.spilnasprava.client.service.cityclient.CityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by YaroslaV on 22.03.2018.
 */
@RestController
@RequestMapping("rest/cities")
public class CityController {

    @Autowired
    private CityClient cityClient;

    @GetMapping
    public List<City> getInformation(@RequestParam ("user") String name){
        return cityClient.getAllCities(name);
    }

}
