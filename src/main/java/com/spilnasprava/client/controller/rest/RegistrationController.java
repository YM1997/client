package com.spilnasprava.client.controller.rest;

import com.spilnasprava.client.model.User;
import com.spilnasprava.client.service.userclient.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by YaroslaV on 22.03.2018.
 */
@RestController
@RequestMapping("registration")
public class RegistrationController {

    @Autowired
    private UserClient userClient;

    @PostMapping("/user")
    public void registrationUser(@RequestBody User user){
        userClient.saveUser(user);
    }
}
