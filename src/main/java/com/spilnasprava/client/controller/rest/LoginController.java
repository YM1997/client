package com.spilnasprava.client.controller.rest;

import com.spilnasprava.client.service.loginclient.LoginClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by YaroslaV on 22.03.2018.
 */
@RestController
@RequestMapping("rest")
public class LoginController {

    @Autowired
    private LoginClient loginClient;

    @PostMapping("/token")
    public void token(@RequestParam ("name") String name, @RequestParam ("password") String password) {loginClient.login(name, password);
    }
}
