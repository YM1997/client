package com.spilnasprava.client.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by YaroslaV on 22.03.2018.
 */
@Controller
public class WebController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = {"/token"}, method = RequestMethod.GET)
    public String getToken(@RequestParam ("name") String name){
        return "token";
    }

}
