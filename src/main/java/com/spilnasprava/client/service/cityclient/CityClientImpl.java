package com.spilnasprava.client.service.cityclient;

import com.spilnasprava.client.model.City;
import com.spilnasprava.client.model.Tokens;
import com.spilnasprava.client.service.loginclient.LoginClientImpl;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by YaroslaV on 22.03.2018.
 */
@Service
public class CityClientImpl implements CityClient {

    final String URI = "http://192.168.2.116:8999/user/cities";

    static Logger log = Logger.getLogger(LoginClientImpl.class.getName());


    @Override
    public List<City> getAllCities(String user) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        log.info("My token "+Tokens.getTokens(user));
        headers.add("Authorization", Tokens.getTokens(user));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<List> response = restTemplate.exchange(URI, HttpMethod.GET, entity, List.class);
        return response.getBody();
    }

}
