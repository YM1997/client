package com.spilnasprava.client.service.cityclient;

import com.spilnasprava.client.model.City;

import java.util.List;

/**
 * Created by YaroslaV on 22.03.2018.
 */
public interface CityClient {
    List<City> getAllCities(String user);

}
