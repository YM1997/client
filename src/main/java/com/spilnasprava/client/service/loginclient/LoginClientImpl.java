package com.spilnasprava.client.service.loginclient;

import com.spilnasprava.client.model.Tokens;
import com.spilnasprava.client.model.User;
import com.spilnasprava.client.response.AuthenticationResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

/**
 * Created by YaroslaV on 22.03.2018.
 */
@Service
public class LoginClientImpl implements LoginClient {

    static Logger log = Logger.getLogger(LoginClientImpl.class.getName());


    final String ROOT_URI = "http://192.168.2.116:8999/permit-all/token";


    @Override
    public void login(String name, String password) {
        User user = new User();
        user.setName(name);
        user.setPassword(password);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<User> entity = new HttpEntity<User>(user);
        ResponseEntity<AuthenticationResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.POST, entity, AuthenticationResponse.class);
        Tokens.setTokens(name, response.getBody().getToken());
    }
}
