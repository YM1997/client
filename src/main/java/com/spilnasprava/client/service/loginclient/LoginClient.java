package com.spilnasprava.client.service.loginclient;

/**
 * Created by YaroslaV on 22.03.2018.
 */
public interface LoginClient {

    void login(String name, String password);
}
