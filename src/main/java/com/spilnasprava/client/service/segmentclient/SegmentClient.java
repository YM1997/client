package com.spilnasprava.client.service.segmentclient;

import org.springframework.data.domain.Page;

import javax.swing.text.Segment;
import java.util.List;


/**
 * Created by YaroslaV on 22.03.2018.
 */
public interface SegmentClient {
    List<Segment> findByCity(String user, String name);
    Page<Segment> findAll(String user,int page, int size);
    List<Segment> findByPoints(String user,double lat, double lon, double radius);
}
