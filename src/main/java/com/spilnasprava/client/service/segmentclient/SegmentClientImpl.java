package com.spilnasprava.client.service.segmentclient;

import com.spilnasprava.client.model.Tokens;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.swing.text.Segment;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by YaroslaV on 22.03.2018.
 */
@Service
public class SegmentClientImpl implements SegmentClient{

    static Logger log = Logger.getLogger(SegmentClientImpl.class.getName());


    final String URI = "http://192.168.2.116:8999/user/segments";


    @Override
    public List<Segment> findByCity(String user, String name) {
        RestTemplate restTemplate = new RestTemplate();
        String newURI=URI+"?city="+name;
        HttpHeaders headers = new HttpHeaders();
        log.info("My token "+ Tokens.getTokens(user));
        headers.add("Authorization", Tokens.getTokens(user));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<List> response = restTemplate.exchange(newURI, HttpMethod.GET, entity, List.class);
        return response.getBody();
    }

    @Override
    public Page<Segment> findAll(String user, int page, int size) {
        RestTemplate restTemplate = new RestTemplate();
        String newURI=URI+"?page="+page+"&size="+size;
        HttpHeaders headers = new HttpHeaders();
        log.info("My token "+ Tokens.getTokens(user));
        headers.add("Authorization", Tokens.getTokens(user));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Page> response = restTemplate.exchange(newURI, HttpMethod.GET, entity, Page.class);
        return response.getBody();
    }

    @Override
    public List<Segment> findByPoints(String user, double lat, double lon, double radius) {
        RestTemplate restTemplate = new RestTemplate();
        String newURI=URI+"?lat="+lat+"&lon="+lon+"&radius="+radius;
        HttpHeaders headers = new HttpHeaders();
        log.info("My token "+ Tokens.getTokens(user));
        headers.add("Authorization", Tokens.getTokens(user));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<List> response = restTemplate.exchange(newURI, HttpMethod.GET, entity, List.class);
        return response.getBody();
    }

}
