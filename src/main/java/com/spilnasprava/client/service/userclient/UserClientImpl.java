package com.spilnasprava.client.service.userclient;

import com.spilnasprava.client.model.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by YaroslaV on 22.03.2018.
 */
@Service
public class UserClientImpl implements UserClient {

    final String ROOT_URI = "http://192.168.2.116:8999/permit-all/registration/user";

    @Override
    public void saveUser(User user) {
        RestTemplate restTemplate= new RestTemplate();
        HttpEntity<User> entity = new HttpEntity<User>(user);
        restTemplate.exchange(ROOT_URI, HttpMethod.POST, entity, User.class);
    }
}
