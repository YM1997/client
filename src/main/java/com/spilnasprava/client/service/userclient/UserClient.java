package com.spilnasprava.client.service.userclient;

import com.spilnasprava.client.model.User;

/**
 * Created by YaroslaV on 22.03.2018.
 */
public interface UserClient {
    void saveUser(User user);

}
