package com.spilnasprava.client.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by YaroslaV on 14.03.2018.
 */

@Setter
@Getter
@NoArgsConstructor
public class City {

    private String name;
    private String prefix;
    private double leftLatitude;
    private double rightLatitude;
    private double leftLongitude;
    private double rightLongitude;
    private String originalName;

}
