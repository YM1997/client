package com.spilnasprava.client.model;

import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by YaroslaV on 23.03.2018.
 */

@NoArgsConstructor
public class Tokens {
    private static Map<String, String> tokens= new HashMap<String,String>();


    public static String getTokens(String name) {
        String responseToken="Token "+tokens.get(name);
        return responseToken;
    }

    public static void setTokens(String key, String token) {
        tokens.put(key,token);
    }
}
