package com.spilnasprava.client.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

/**
 * Created by YaroslaV on 06.03.2018.
 */

@Getter
@Setter
@ToString
@NoArgsConstructor
public class User {

    private String name;
    private String password;
    private Timestamp registered;
    private Timestamp updated;
    private String role;

}
