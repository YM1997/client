package com.spilnasprava.client.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * Created by YaroslaV on 13.03.2018.
 */


@Getter
@Setter
@NoArgsConstructor
public class AuthenticationResponse {
    private HttpStatus status;
    private String massage;
    private String token;
}
